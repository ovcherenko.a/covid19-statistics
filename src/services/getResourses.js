
class GetResourses {

    _baseUrl = 'https://corona.lmao.ninja/v2/historical'

    async getAllResourses() {
        let res = await fetch(this._baseUrl)

        if(!res.ok) {
            throw new Error(`Colud not get information`)
        }

        return await res.json()
    }

    async getOneCountry(number) {
        let countriesData = await this.getAllResourses()
        let countrieData = {}

        countrieData.labels = await Object.keys(countriesData[number].timeline.cases)
        
        let cases = {}
        cases.label = 'Cases'
        cases.data = await Object.values(countriesData[number].timeline.cases)
        cases.backgroundColor = 'rgba(252,113,0,0.5)';
    
        let recovered = {}
        recovered.label = 'Recovered'
        recovered.data = await Object.values(countriesData[number].timeline.recovered)
        recovered.backgroundColor = 'rgba(129,188,14,0.5)';
    
        let deaths = {}
        deaths.label = 'Deaths'
        deaths.data = await Object.values(countriesData[number].timeline.deaths)
        deaths.backgroundColor = 'rgba(0,0,0,0.5)';

        countrieData.datasets = [];
        countrieData.datasets.push(recovered)
        countrieData.datasets.push(deaths)
        countrieData.datasets.push(cases)


        return await countrieData
    }

}

export default GetResourses