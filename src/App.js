import React, { Component } from 'react';
import './App.css';
import '../node_modules/react-vis/dist/style.css'
import { Line } from 'react-chartjs-2'

import ChartComponent from './components/ChartComponent'

const url = 'https://corona.lmao.ninja/v2/historical'
class App extends Component {
  constructor() {
    super();
    this.state = {
      selectedCountry: "Select country",
      components: [<ChartComponent 
                    key={Math.random()}
                    id={0} 
                    deleteChart={this.deleteChart.bind(this)}/>]
    }
  }

  renderSelectElements() {
    let optionElements = []
    for(let j = 0; j < this.state.allCountries.length; j++) {
      optionElements.push( <option key={Math.random()*20} value={this.state.allCountries[j].country} tabIndex={j}>{this.state.allCountries[j].country}</option> )
    }
    this.setState({
      optionElements
    })
  }

  selectCountry(e) {
    for( let i = 0; i< e.target.length; i++) {
      if(e.target[i].selected) {
        this.setState({
          selectedCountry: e.target[i].value
        })
        this.showCountry(e.target[i].tabIndex)
      }
    }
  }

  showCountry(number) {
    let countriesData = []
    console.log(number)

    let countrieData = {}

    countrieData.labels = Object.keys(this.state.allCountries[number].timeline.cases);

    let cases = {}
    cases.label = 'Cases'
    cases.data = Object.values(this.state.allCountries[number].timeline.cases)
    cases.backgroundColor = 'rgba(252,113,0,0.5)';

    let recovered = {}
    recovered.label = 'Recovered'
    recovered.data = Object.values(this.state.allCountries[number].timeline.recovered)
    recovered.backgroundColor = 'rgba(129,188,14,0.5)';

    let deaths = {}
    deaths.label = 'Deaths'
    deaths.data = Object.values(this.state.allCountries[number].timeline.deaths)
    deaths.backgroundColor = 'rgba(0,0,0,0.5)';

    countrieData.datasets = [];
    countrieData.datasets.push(recovered)
    countrieData.datasets.push(deaths)
    countrieData.datasets.push(cases)

    countriesData.push(<Line 
                          key={Math.random()*20}
                          data={countrieData}
                          height={700}
                          width={1000}
                        />)


    this.setState({
      countriesData
    })
                    
  }

  addNewChart() {
    let list = [...this.state.components]
    list.push( <ChartComponent 
      id={list.length} 
      key={Math.random()}
      deleteChart={this.deleteChart.bind(this)} /> )
    this.setState({
      components: list
    })
  }

  deleteChart(number){
    // console.log(number)
    let list = [...this.state.components]
    list.splice(number, 1)
    this.setState({
      components:list
    })
  }


  // componentDidMount() {
  //   fetch(url)
  //     .then(response => response.json())
  //     .then(data => {
  //                 this.setState({
  //                   allCountries: [...data]
  //                 })
  //                 this.renderSelectElements()
  //                 return data
  //     })
  //   }

  render() {

    return (
      <div className="App">
        <h2>covid-19</h2>
        <h4>statistics</h4>
{/* 
        <select value={this.state.selectedCountry} onChange={this.selectCountry.bind(this) }>
          <option disabled>Select country</option>
          {this.state.optionElements}
        </select> */}

        {/* <CountryStatChartJS data={this.state.countriesData}/> */}


        {this.state.components}

        <div className="add-chart" onClick={this.addNewChart.bind(this)}>
          Добавить таблицу
        </div>


{/* 
      <ChartComponent /> */}

      </div>
    );
  }
}

export default App;
