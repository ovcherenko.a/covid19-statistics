import React, { Component  } from 'react'
import {Line} from 'react-chartjs-2'
import GetResourses from './../../services/getResourses'
import './ChartComponent.css'

class ChartComponent extends Component{    
    constructor(props) {
        super(props);
        this.getRes = new GetResourses();
        this.id = props.id;
        this.deleteChart = props.deleteChart;
        this.state = { 
            selectedCountry: '',
            optionElements: null,
            deathsForLastDay: 0
        }
    }

    componentDidMount() {
        this.getRes
            .getAllResourses()
                .then(data => {
                    let nameList = data.map(item => {
                        return( <option
                                    key={Math.random()}>
                                    {item.country}
                                </option>
                        )
                    })

                    this.setState({
                        optionElements: nameList 
                    }
                )
            }
        )
    }


    selectCountry(e) {
        e.preventDefault()
        let target = e.target.childNodes;
        let index = null
        target.forEach((element, indexLocal) => {
            if(element.selected){
                index = indexLocal;
            }
        })

        this.getRes
            .getOneCountry(index)
            .then((dataCountry) => {
                this.setState({
                    selectedCountry: <Line 
                                        data={dataCountry}
                                        height={700}
                                        width={1000}
                                    />
                    }
                )    
                return dataCountry
            })
            .then(data => {
                let deaths = data.datasets[2].data[data.datasets[2].data.length-1] - data.datasets[2].data[data.datasets[2].data.length-2]
                this.setState({
                    deathsForLastDay: deaths
                })
            }
        )
    }

    render() {
        return(
            <div style={{
                border: "1px solid #444",
                padding: "50px",
                width: "1000px",
                margin: "50px",
                position: 'relative',
                borderRadius: '30px'
            }}>

                <div onClick={this.props.deleteChart} className="close-btn">close</div>
                <select defaultValue={'Select country'} onChange={this.selectCountry.bind(this)}>
                    <option disabled>Select country</option>
                    {this.state.optionElements}
                </select>
                <div className="chart-wrapper">
                    <p>
                        {`За вчера заболело: ${this.state.deathsForLastDay}`} 
                    </p>
                    <p>
                        {this.state.selectedCountry}
                    </p>
                </div>

            </div>
        )
    }
}

export default ChartComponent