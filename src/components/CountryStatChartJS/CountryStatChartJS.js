import React, {Component} from 'react'
import './CountryStatChartJS.css'

class CountryStatChartJS extends Component {
    constructor(props){ 
        super(props)
        this.state = {}
    }
    render(){
        
        return(
            <div className="chart-wrapper">
                <p>
                    {this.props.data}
                </p>
            </div>
        )
    }
}

export default CountryStatChartJS;